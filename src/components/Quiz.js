import React from 'react'
import QuestionDisplay from "./QuestionDisplay";

const Quiz = ({requested, loaded, quizData}) => {

    //formats data received from API to make it easier to work with
    function formatQuizData(quizData) {
        //data received from the API
        console.log(quizData)
        //allQuestions is an array that is populated with individual "question" objects,
        //that contain question and answers to it
        let allQuestions = quizData.results.map((x,i) => formatQuizQuestion(quizData, i))
        //same data, formatted
        console.log(allQuestions)
        return allQuestions
    }

    //this function processes individual "question" objects received from API
    function formatQuizQuestion(quizData, i) {
        //creates an object consisting of answer text and information whether the answer is correct
        //as multiple incorrect answers are received, and only one correct, its easier to start with
        //using .map to create and populate array of answer objects
        let incorrectAnswers = Object.assign(quizData.results[i].incorrect_answers.map(x => ({ ["answerText"]: decodeHTMLEntities(x), ["isCorrect"]: false }) ))
        //similarly answer object is created for correct answer
        let correctAnswer = Object.assign({ ["answerText"]: decodeHTMLEntities(quizData.results[i].correct_answer), ["isCorrect"]: true })
        //correct answer is added to existing array with incorrect answers, creating complete answer set
        incorrectAnswers.push(correctAnswer)
        //answers are shuffled, so that position of correct answer is unpredictable
        let allAnswers = shuffle(incorrectAnswers)
        //question text is retrieved from the API data
        let questionText = decodeHTMLEntities(quizData.results[i].question)
        //complete "question" object is created from answers array and question text
        let completeQuestion = Object.assign({["questionText"]: questionText, ["answerOptions"]: allAnswers})
        return completeQuestion
    }

    // Fisher-Yates Shuffle algorithm to get unbiased shuffle results
    function shuffle(array) {
        let currentIndex = array.length, temporaryValue, randomIndex;
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    }

    //this function decodes HTML entities that are encoded in data received from the API (i.e "&amp;" -> "&")
    function decodeHTMLEntities(text) {
        let textArea = document.createElement('textarea');
        textArea.innerHTML = text;
        return textArea.value;
    }
    //if user has pressed "generate quiz" button, but data has not loaded from the API yet
    if (requested && !loaded) return (
        <div className="quiz">
            Loading the quiz...
        </div>
    )
    //if user has pressed "generate quiz" button and the data from the API is received, quiz is displayed
    if (requested && loaded) return (
        <div className="quiz">
            <QuestionDisplay
                allQuestions={formatQuizData(quizData)}
            />
        </div>
    )
    //before user pressed "generate quiz" button nothing is displayed
    else return (
        <div></div>
    )
}
export default Quiz