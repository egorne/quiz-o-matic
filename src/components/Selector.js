import React, {useState} from 'react'
import axios from 'axios'
import Quiz from './Quiz'

const Selector = ({}) => {
    const [category, setCategory] = useState("9")
    const [difficulty, setDifficulty] = useState("medium")
    const [amount, setAmount] = useState("10")
    const [requested, setRequested] = useState(false)
    const [quizData, setQuizData] = useState([])
    const [loaded, setLoaded] = useState(false)

    // checks if user has pressed "generate quiz" button, quiz area is hidden until the button is pressed
    function quizRequested() {
        setRequested(true)
    }

    // requests quiz questions from the API
    function getQuizData() {
        // checks if data is loaded from API to stop functions that rely on that data from running
        // and to set visible element that informs user that quiz is currently loading
        setLoaded(false);
        axios
            .get(`https://opentdb.com/api.php?amount=${amount}&category=${category}&difficulty=${difficulty}&type=multiple`)
            .then(response => {
                setQuizData(response.data)
                setLoaded(true)
                })
    }

    return (
        <div className="selector">
            <div id="logo">
                    <p>Quiz-O-Matic</p>
            </div>
            <form>
                <label htmlFor="Category">Category: </label><br/>
                <select
                    name="Category"
                    defaultValue={"9"}
                    onChange={e => setCategory(e.target.value)}
                >
                    <option value="9">General Knowledge</option>
                    <option value="10">Entertainment: Books</option>
                    <option value="11">Entertainment: Film</option>
                    <option value="12">Entertainment: Music</option>
                    <option value="15">Entertainment: Video Games</option>
                    <option value="18">Science: Computers</option>
                </select>
                <br/>
                <label htmlFor="Category">Difficulty: </label><br/>
                <select
                    name="difficulty"
                    defaultValue={"medium"}
                    onChange={e => setDifficulty(e.target.value)}
                >
                    <option value="easy">Easy</option>
                    <option value="medium">Medium</option>
                    <option value="hard">Hard</option>
                </select>
                <br/>
                <label htmlFor="Amount">Amount of questions: </label><br/>
                <select
                    name="amount"
                    defaultValue={"10"}
                    onChange={e => setAmount(e.target.value)}
                >
                    <option value="4">4</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                </select>
            </form>
            <br/>
            <button
                onClick={e => {
                quizRequested();
                getQuizData();
            }}>
                Generate Quiz
            </button>
            <br/>
            <Quiz
                requested={requested}
                loaded={loaded}
                quizData={quizData}
            />
        </div>
    )
}
export default Selector