import React, { useState } from 'react'

const QuestionDisplay = ({allQuestions}) => {

    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [showScore, setShowScore] = useState(false);
    const [score, setScore] = useState(0);

    const handleAnswerOptionClick = (isCorrect) => {
        //increments score by 1 if the answer user selected was correct
        if (isCorrect) {
            setScore(score + 1);
        }

        //renders next question visible, if it was the last question, sets component to show score instead
        const nextQuestion = currentQuestion + 1;
        if (nextQuestion < allQuestions.length) {
            setCurrentQuestion(nextQuestion);
        } else {
            setShowScore(true);
        }
    };

    return (
        <div>
            {/*displays score once user answers the last question*/}
            {showScore ? (
                <div className='score-section'>
                    You scored {score} out of {allQuestions.length}
                </div>
            ) : (
                // displays questions until user answers last question
                <div>
                    <div className='question-section'>
                        <div className='question-count'>
                            <span>Question {currentQuestion + 1}</span>/{allQuestions.length}
                        </div>
                        <div className='question-text'>{allQuestions[currentQuestion].questionText}</div>
                    </div>
                    <div className='answer-section'>
                        {/*automatically generates an html element for every item in answer array*/}
                        {allQuestions[currentQuestion].answerOptions.map((answerOption,i) => (
                            <button key={i} onClick={() => handleAnswerOptionClick(answerOption.isCorrect)}>{answerOption.answerText}</button>
                        ))}
                    </div>
                </div>
            )}
        </div>
    )
}
export default QuestionDisplay