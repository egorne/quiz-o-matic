import './App.css';
import Particles from 'react-particles-js';
import Selector from './components/Selector'
import Quiz from './components/Quiz'
import React from 'react';

function App() {
  return (
    <div className="App">
        {/*Particles module creates background animation*/}
        <Particles className="tsparticles" />
        <Selector />
        <Quiz />
    </div>
  );
}

export default App;
